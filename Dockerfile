FROM gradle

COPY exclude.xml include.xml build.gradle /fsb/
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
